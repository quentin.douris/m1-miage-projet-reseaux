import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

/**
 * UDP
 * Classe qui gère la communication selon le protocol de communication UDP
 * @author Quentin Douris
 */
public class UDP {

    private int port;
    private Comprehension comprehension;


    /**
     * Constructeur de UDP
     * @param port : numéro de port nécessaire à la communication
     * @param comprehension : objet qui permet la compréhension
     */
    public UDP(int port, Comprehension comprehension) {
        this.port = port;
        this.comprehension = comprehension;
    }


    /**
     * Réalise le travrail du protocol UDP
     */
    public void travail() {
        try {
            // DatagramSocket auquel on précise le port pour recevoir les messages
            DatagramSocket s = new DatagramSocket(port);

            // Prépareration de la réception
            byte buffer[] = new byte[1024];
            DatagramPacket dp = new DatagramPacket(buffer, buffer.length);

            // Réception
            s.receive(dp);

            // Extraction des donneés reçu
            String requete = new String(dp.getData(), 0, dp.getLength());

            // Réponse de la partie métier
            String reponse = this.comprehension.traiter(requete);

            // Construction du packet de retour
            DatagramPacket dpRetour = new DatagramPacket(reponse.getBytes(), reponse.getBytes().length, dp.getAddress(), dp.getPort());

            // Envoi de la réponse
            s.send(dpRetour);

            // Fermture du socket
            s.close();
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
