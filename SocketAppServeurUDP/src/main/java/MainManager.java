/**
 * Main
 * Classe qui jouer le rôle du programme principal du client UDP Checker
 *
 * @author Quentin Douris
 */
public class MainManager {

    /**
     * Programme principal
     * @param args : arguments
     */
    public static void main(String[] args) {
        int port = 28415;
        Metier metier = new Metier();
        Comprehension comprehension = new Comprehension(metier, port);
        UDP udp = new UDP(port, comprehension);
        udp.travail();
    }
}
