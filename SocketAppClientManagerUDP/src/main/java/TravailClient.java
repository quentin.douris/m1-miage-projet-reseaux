import java.io.IOException;
import java.net.*;

/**
 * TravailClient
 *
 * @author Quentin Douris
 */
public class TravailClient {

    /**
     * entier qui permet de stocker le numéro de port
     */
    private int port;


    /**
     * Contructeur de la classe TavailClient
     * @param port
     */
    public TravailClient(int port) {
        this.port = port;
    }


    /**
     * Réalisae le travail du protocol UDP
     * @param requete : requête envoyé par le client
     */
    public void travail(String requete) {
        try {
            // Contruction de la socket
            DatagramSocket ds = new DatagramSocket();

            // Construction du paquet à envoyer
            DatagramPacket dp = new DatagramPacket(requete.getBytes(), requete.getBytes().length, InetAddress.getLocalHost(), this.port);

            // Envoi du paquet
            ds.send(dp);

            // Préparation de la réception de la réponse
            byte[] messageReponse = new byte[1024];
            DatagramPacket dpReponse = new DatagramPacket(messageReponse, messageReponse.length);
            ds.receive(dpReponse);

            // Récupération de la réponse
            String reponse = new String(dpReponse.getData(),0, dpReponse.getLength());

            // Affichage de la réponse à l'utilisateur
            System.out.println("Réponse serveur UDP : " + reponse);

            // Fermeture de l'envoi
            ds.close();
        } catch (SocketException ex) {
            ex.printStackTrace();
        } catch (UnknownHostException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
