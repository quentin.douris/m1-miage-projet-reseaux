/**
 * Main
 * Classe du programme principal de l'application cliente UDP Manager
 *
 * @author Quentin Douris
 */
public class Main {

    /**
     * Constrante qui correspond au numéro de port du client Manager
     */
    private static final int PORT_MANAGER = 28415;

    /**
     * Programme principal
     * @param args : arguments
     */
    public static void main(String[] args) {
        String requete = "DEL Toto Toto";

        // Envoi de la requête
        TravailClient travailClient = new TravailClient(PORT_MANAGER);
        travailClient.travail(requete);
    }

}
