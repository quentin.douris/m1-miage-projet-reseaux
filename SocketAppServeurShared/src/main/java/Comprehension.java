import javax.print.DocFlavor;

/**
 * Comprehension
 *
 * @author Quentin Douris
 */
public class Comprehension {

    /**
     * constante qui indique le nombre de paramètre requis pour réaliser une requête
     */
    private final int NB_PARAMETRE_REQUETE = 3;

    /**
     * constante qui représente le paramètre assigné à une requête de vérification
     */
    private final String REQ_CHECK = "CHK";

    /**
     * constante qui représente le paramètre assigné à une requête d'ajout
     */
    private final String REQ_ADD = "ADD";

    /**
     * constante qui représente le paramètre assigné à une requête de suppression
     */
    private final String REQ_DELETE = "DELL";

    /**
     * constante qui représente le paramètre assigné à une requête de mise à jour
     */
    private final String REQ_UPDATE = "MAJ";

    /**
     * constante qui représente la valeur d'une réponse correcte
     */
    private final String REP_CORRECTE = "GOOD";

    /**
     * constante qui représente la valeur d'une réponse incorrecte
     */
    private final String REP_INCORRECTE = "BAD";

    /**
     * constante qui représente la valeur du port réservé au client CHECKER
     */
    private final int PORT_CHECKER = 28414;

    /**
     * constante qui représente la valeur du port réservé au client MANAGE
     */
    private final int PORT_MANAGER = 28415;

    /**
     * Objet qui permet de stocker le métier
     */
    private Metier metier;

    /**
     * Objet qui permet d'identifier le client Manager
     */
    private boolean managerClient;

    /**
     * Objet qui permet d'identifier le client Checker
     */
    private boolean checkerClient;


    /**
     * Constructeur de comprehension
     * @param metier : objet metier
     */
    public Comprehension(Metier metier, int port) {
        this.metier = metier;
        // Identification du type de client
        this.managerClient = port == PORT_MANAGER;
        this.checkerClient = port == PORT_CHECKER;
    }


    /**
     * traite les différentes requêtes qui sont envoyées
     * @param requete : la requête envoyée
     * @return
     */
    public String traiter(String requete) {
        // Configuration de la réponse par défaut
        String reponse = "ERROR : Traitement impossible !";

        // Découpe la requête selon le séparateur défini
        String[] tabRequete = requete.split(" ");

        // Vérifier le nombre de paramètre
        if (tabRequete.length == NB_PARAMETRE_REQUETE) {
            // Récupération des différentes informations de la requête
            String commande = tabRequete[0].toUpperCase();
            String login = tabRequete[1];
            String password = tabRequete[2];

            // Exécution selon la commande
            switch (commande) {
                case REQ_CHECK: {
                    if (this.managerClient || this.checkerClient) {
                        // Appel à la classe Métier pour obtenir la réponse
                        boolean retourMetier = this.metier.tester(login,password);

                        // Formatage de la réponse
                        if (retourMetier) {
                            reponse = REP_CORRECTE;
                        }
                        else {
                            reponse = REP_INCORRECTE;
                        }
                    }
                    else {
                        reponse = "ERROR : Permission insuffisante";
                    }

                    break;
                }

                case REQ_ADD: {
                    if (this.managerClient) {
                        // Appel à la classe Métier pour obtenir la réponse
                        boolean retourMetier = this.metier.creer(login,password);

                        // Formatage de la réponse
                        if (retourMetier) {
                            reponse = REP_CORRECTE;
                        }
                        else {
                            reponse = REP_INCORRECTE;
                        }
                    }
                    else {
                        reponse = "ERROR : Permission insuffisante";
                    }
                    break;
                }

                case REQ_DELETE: {
                    if (this.managerClient) {
                        // Appel à la classe Métier pour obtenir la réponse
                        boolean retourMetier = this.metier.supprimer(login, password);

                        // Formatage de la réponse
                        if (retourMetier) {
                            reponse = REP_CORRECTE;
                        } else {
                            reponse = REP_INCORRECTE;
                        }
                    }
                    else {
                        reponse = "ERROR : Permission insuffisante";
                    }
                    break;
                }

                case REQ_UPDATE: {
                    if (this.managerClient) {
                        // Appel à la classe Métier pour obtenir la réponse
                        boolean retourMetier = this.metier.mettreAJour(login, password);

                        // Formatage de la réponse
                        if (retourMetier) {
                            reponse = REP_CORRECTE;
                        } else {
                            reponse = REP_INCORRECTE;
                        }
                    }
                    else {
                        reponse = "ERROR : Permission insuffisante";
                    }

                    break;
                }
            }
        }
        return reponse;
    }

}
