/**
 * Main
 * Classe qui jouer le rôle du programme principal du client TCP Checker
 *
 * @author Quentin Douris
 */
public class MainChecker {

    /**
     * Programme principal
     * @param args arguments
     */
    public static void main(String[] args) {
        int port = 28414;
        Metier metier = new Metier();
        Comprehension comprehension = new Comprehension(metier, port);
        TCP tcp = new TCP(port ,comprehension);
        tcp.travail();
    }
}
