import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * TCP
 * Classe qui gére la communication
 *
 * @author Quentin Douris
 */
public class TCP {

    /**
     * Integer qui stock le numéro de port utilisé pour la communication
     */
    private int port;

    /**
     * Objet Comprehension qui permet d'interpréter le résultat des requêtes
     */
    private Comprehension comprehension;



    /**
     * Constructeur de TCP
     * @param port : numéro de port nécessaire à la communication
     * @param comprehension : objet qui permet la compréhesion des requête
     */
    public TCP(int port, Comprehension comprehension) {
        this.port = port;
        this.comprehension = comprehension;
    }



    /**
     * Réalise le travail du protocol TCP
     */
    public void travail() {
        try {
            // Ouverture du service
            ServerSocket sEcoute = new ServerSocket(port);

            // Attente du client
            Socket sService = sEcoute.accept();

            try {
                // Gestion du client
                BufferedReader entreeSocket = new BufferedReader(new InputStreamReader(sService.getInputStream()));
                PrintStream sortiSocket = new PrintStream(sService.getOutputStream());
                String requete = entreeSocket.readLine();
                String reponse = comprehension.traiter(requete);
                sortiSocket.println(reponse);
            } catch (IOException ex) {
                Logger.getLogger(TCP.class.getName()).log(Level.SEVERE, null, ex);
            }

            // Fermeture du service
            sService.close();
            sEcoute.close();
        } catch (IOException ex) {
            Logger.getLogger(TCP.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
