# M1 MIAGE - Projet Réseaux

Développement de façon incrémentale d'un service simplifié d'authentification basée sur un "Login/Password".

Le répertoire est constitué de 4 projets :
- **SocketAppServeurShared** : Contient les classes java partagées entre les différents serveurs.
- **SocketAppServeurTCP** : Application de serveur TCP.
- **SocketAppServeurUDP** : Application de serveur UDP.
- **SocketAppClient** : Application client.
