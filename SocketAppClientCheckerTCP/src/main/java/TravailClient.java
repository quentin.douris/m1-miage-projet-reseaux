import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Travail client
 *
 * @author Quentin Douris
 */
public class TravailClient {

    /**
     * entier qui permet de stocket le numéro de port
     */
    private int port;


    /**
     * Constructeur de la classe port
     * @param port : numéro de port
     */
    public TravailClient(int port) {
        this.port = port;
    }


    /**
     *
     */
    public void travail(String requete) {
        try {

            Socket socket = new Socket("localhost", port);

            try {
                // Gestion du client
                BufferedReader entreeSocket = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                PrintStream sortiSocket = new PrintStream(socket.getOutputStream());
                sortiSocket.println(requete);

                String reponse = entreeSocket.readLine();
                System.out.println("Réponse du serveur : " + reponse);

            } catch (IOException ex) {
                Logger.getLogger(TravailClient.class.getName()).log(Level.SEVERE, null, ex);
            }

            socket.close();

        } catch (IOException ex) {
            Logger.getLogger(TravailClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
