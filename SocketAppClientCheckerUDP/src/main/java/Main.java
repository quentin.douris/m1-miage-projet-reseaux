/**
 * Main
 * Classe du programme principal de l'application cliente UDP Checker
 *
 * @author Quentin Douris
 */
public class Main {

    /**
     * Constrante qui correspond au numéro de port du client Checker
     */
    private static final int PORT_CHECKER = 28414;

    /**
     * Programme principal
     * @param args : arguments
     */
    public static void main(String[] args) {
        String requete = "CHK Toto Toto";

        // Envoi de la requête
        TravailClient travailClient = new TravailClient(PORT_CHECKER);
        travailClient.travail(requete);
    }


}
