import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Travail client
 *
 * @author Quentin Douris
 */
public class TravailClient {

    /**
     * entier qui permet de stocket le numéro de port
     */
    private int port;


    /**
     * Constructeur de la classe port
     * @param port : numéro de port
     */
    public TravailClient(int port) {
        this.port = port;
    }


    /**
     *
     */
    public void travail(String requete) {

    }


}
