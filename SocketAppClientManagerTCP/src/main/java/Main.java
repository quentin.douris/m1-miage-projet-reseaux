/**
 * Main
 *
 *
 * @author Quentin Douris
 */
public class Main {

    /**
     * Constante qui correspond au numéro de port du client Manager
     */
    private static final int PORT = 28415;

    /**
     * Programme principal
     * @param args arguments
     */
    public static void main(String[] args) {
        String requete = "ADD Toto Toto";

        // Envoi de la requête
        TravailClient travailClient = new TravailClient(PORT);
        travailClient.travail(requete);
    }

}
